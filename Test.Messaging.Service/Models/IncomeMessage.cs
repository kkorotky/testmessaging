﻿using System.Collections.Generic;

namespace Test.Messaging.Service.Models
{
	public class IncomeMessage
	{
		public IncomeMessage()
		{
			Recipients = new List<string>();
		}

		public List<string> Recipients { get; set; }
		public string Subject { get; set; }
		public string Body { get; set; }
	}
}
