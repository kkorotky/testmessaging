﻿namespace Test.Messaging.Service
{
	public interface IIdGeneratorService
	{
		string GenerateId();
	}
}
