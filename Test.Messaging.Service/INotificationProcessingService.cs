﻿using Test.Messaging.Data.Dto;

namespace Test.Messaging.Service
{
	public interface INotificationProcessingService
	{
		void ProcessNotification(MessageDto message);
	}
}
