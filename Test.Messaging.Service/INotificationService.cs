﻿using Test.Messaging.Data.Dto;

namespace Test.Messaging.Service
{
	public interface INotificationService
	{
		void Init();
		void Stop();
		void SendDelayedNotification(MessageDto message);
	}
}
