﻿using System;
using System.Collections;
using System.Threading;
using System.Threading.Tasks;
using Test.Messaging.Common;
using Test.Messaging.Data.Dto;

namespace Test.Messaging.Service.Implementation
{
	public class NotificationService : INotificationService
	{
		private Task _notificationThread;
		private readonly Queue _messageQueue;
		private bool _isInit;
		private bool _isStop;
		private const int SleepTimeSeconds = 3;

		private readonly ITestLogger _logger;
		private readonly INotificationProcessingService _notificationProcessingService;
		
		private static readonly object LockObj = new object();

		public NotificationService(ITestLogger logger, INotificationProcessingService notificationProcessingService)
		{
			_logger = logger;
			_notificationProcessingService = notificationProcessingService;
			_messageQueue = new Queue();
		}

		public void SendDelayedNotification(MessageDto message)
		{
			_messageQueue.Enqueue(message);
		}

		public void Init()
		{
			// Double-checked locking just in case
			if (!_isInit)
			{
				lock (LockObj)
				{
					if (!_isInit)
					{
						_notificationThread = new Task(NotificationProcessing);
						_notificationThread.Start();

						_isInit = true;
					}
				}
			}
		}

		public void Stop()
		{
			_isStop = true;
		}

		private void NotificationProcessing()
		{
			while (!_isStop)
			{
				try
				{
					if (_messageQueue.Count == 0)
					{
						continue;
					}
					var currMessage = _messageQueue.Dequeue() as MessageDto;

					// Some notification logic here
					_notificationProcessingService.ProcessNotification(currMessage);
				}
				catch (Exception ex)
				{
					_logger.Error("Error during notification processing", ex);
				}

				Thread.Sleep(SleepTimeSeconds * 1000);
			}

			_logger.Info("Notification thread stopped");
		}
	}
}
