﻿using System;
using System.Threading;
using Test.Messaging.Common;
using Test.Messaging.Data;
using Test.Messaging.Data.Dto;

namespace Test.Messaging.Service.Implementation
{
	public class NotificationProcessingService : INotificationProcessingService
	{
		private readonly IMessageRepository _messageRepository;
		private readonly ITestLogger _logger;

		public NotificationProcessingService(IMessageRepository messageRepository, ITestLogger logger)
		{
			_messageRepository = messageRepository;
			_logger = logger;
		}

		public void ProcessNotification(MessageDto message)
		{
			try
			{
				// Emaulte some delay in processing
				Thread.Sleep(1000);

				if (DateTime.Now.Second % 2 == 0)
				{
					throw new Exception("Unexpected error emulated during notification processing");
				}

				

				// Message send
				MessageDto updated = _messageRepository.GetById(message.Id);
				updated.IsSend = true;
				_messageRepository.Update(updated);

				_logger.Info($"Notification processing: Message with ID {message.Id} was send successfully");
			}
			catch (Exception ex)
			{
				_logger.Error($"Notification processing: Failed to deliver message with ID {message.Id}", ex);

				MarkMessageAsFailed(message, ex);
			}
		}

		private void MarkMessageAsFailed(MessageDto message, Exception ex)
		{
			MessageDto updated = _messageRepository.GetById(message.Id);
			updated.IsSendFailed = true;
			updated.LastSendError = ex.Message;
			_messageRepository.Update(updated);
		}
	}
}
