﻿using System;

namespace Test.Messaging.Service.Implementation
{
	public class IdGeneratorService : IIdGeneratorService
	{
		public string GenerateId()
		{
			// Extremely random implementation!
			return Guid.NewGuid().ToString();
		}
	}
}
