﻿using Test.Messaging.Common;
using Test.Messaging.Data;
using Test.Messaging.Data.Dto;
using Test.Messaging.Service.Models;

namespace Test.Messaging.Service.Implementation
{
	public class MessageService : IMessageService
	{
		private readonly IIdGeneratorService _idGeneratorService;
		private readonly IMessageRepository _messageRepository;
		private readonly INotificationService _notificationService;
		private readonly ITestLogger _logger;
		

		public MessageService(IIdGeneratorService idGeneratorService, IMessageRepository messageRepository, ITestLogger logger
						, INotificationService notificationService)
		{
			_idGeneratorService = idGeneratorService;
			_messageRepository = messageRepository;
			_logger = logger;
			_notificationService = notificationService;
		}

		public ResponseMessage SendMessage(IncomeMessage message)
		{
			// Generate new id
			string newId = _idGeneratorService.GenerateId();

			_logger.Info($"Message with Id {newId} received");

			var messageDto = new MessageDto
			{
				Id = newId,
				Recipients = message.Recipients,
				Body = message.Body,
				Subject = message.Subject,
			};

			_messageRepository.Add(messageDto);

			// No matter if request to notification service succeeded or not return a message id immediately.
			_notificationService.SendDelayedNotification(messageDto);

			_logger.Info($"Message with Id {newId} processed");

			return new ResponseMessage
			{
				Id = newId,
			};
		}
	}
}
