﻿using Test.Messaging.Service.Models;

namespace Test.Messaging.Service
{
	public interface IMessageService
	{
		ResponseMessage SendMessage(IncomeMessage message);
	}
}
