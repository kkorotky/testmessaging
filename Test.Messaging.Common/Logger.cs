﻿using System;
using NLog;

namespace Test.Messaging.Common
{
	// Yeah test logger not to have common name with nlog type Logger
	public class TestLogger : ITestLogger
	{
		private static readonly Logger NLogLogger = LogManager.GetLogger("Test.Messaging");

		public void Info(string message)
		{
			NLogLogger.Log(LogLevel.Info, message);
		}

		public void Error(string message, Exception ex = null)
		{
			NLogLogger.Error(ex, message);
		}
	}
}
