﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Test.Messaging.Common;
using Test.Messaging.Data;
using Test.Messaging.Data.Dto;
using Test.Messaging.Service;
using Test.Messaging.Service.Implementation;
using Test.Messaging.Service.Models;

namespace Test.Messaging.Tests
{
	[TestClass]
	public class MessageServiceTests
	{
		[TestMethod]
		public void TestMessageSend_Success()
		{
			// Arrange
			var generatorMock = new Mock<IIdGeneratorService>();
			var messageRepositoryMock = new Mock<IMessageRepository>();
			var loggerMock = new Mock<ITestLogger>();
			var notificationServiceMock = new Mock<INotificationService>();
			
			var target = new MessageService(generatorMock.Object, messageRepositoryMock.Object, loggerMock.Object, notificationServiceMock.Object);

			string targetId = new Guid("2e2c368b-0c2f-4b22-b3dc-3ae85be23943").ToString();
			generatorMock.Setup(s => s.GenerateId()).Returns(targetId);

			var sampleIncomeMessage = new IncomeMessage
			{
				Subject = "test subject",
				Body = "test body",
				Recipients = new List<string> { "someA", "someB" }
			};

			// Act
			ResponseMessage actualMessage = target.SendMessage(sampleIncomeMessage);

			// Assert
			Assert.AreEqual(targetId, actualMessage.Id);
			messageRepositoryMock.Verify(v=>v.Add(It.IsAny<MessageDto>()), Times.Once);
			notificationServiceMock.Verify(v=>v.SendDelayedNotification(It.IsAny<MessageDto>()), Times.Once());
		}
	}
}
