﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Test.Messaging.Service;
using Test.Messaging.Service.Models;
using Test.Messaging.Web.Controllers;

namespace Test.Messaging.Tests
{
	[TestClass]
	public class MessageControllerTest
	{
		[TestMethod]
		public void TestSend()
		{
			// Arrange
			var messageServiceMock = new Mock<IMessageService>();
			var target = new MessageController(messageServiceMock.Object);

			var sampleIncomeMessage = new IncomeMessage
			{
				Subject = "test subject",
				Body = "test body",
				Recipients = new List<string> { "someA", "someB" }
			};

			// Act
			target.Send(sampleIncomeMessage);

			// Assert
			messageServiceMock.Verify(v=>v.SendMessage(It.IsAny<IncomeMessage>()), Times.Once);
		}
	}
}
