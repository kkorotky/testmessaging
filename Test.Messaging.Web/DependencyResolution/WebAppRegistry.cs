using Test.Messaging.Common;
using Test.Messaging.Service;
using Test.Messaging.Data;
using Test.Messaging.Service.Implementation;

namespace Test.Messaging.Web.DependencyResolution
{
	public class WebAppRegistry : StructureMap.Registry
	{
		#region Constructors and Destructors

		public WebAppRegistry()
		{
			Scan(
				scan =>
				{
					scan.TheCallingAssembly();
					scan.AssemblyContainingType<IMessageService>();
					scan.AssemblyContainingType<IMessageRepository>();
					scan.AssemblyContainingType<ITestLogger>();
					
					scan.WithDefaultConventions();
				});

			// Yeah, singleton!
			ForSingletonOf<INotificationService>().Use<NotificationService>();
		}

		#endregion
	}
}