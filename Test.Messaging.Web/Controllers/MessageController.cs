﻿using System.Web.Http;
using Test.Messaging.Service;
using Test.Messaging.Service.Models;

namespace Test.Messaging.Web.Controllers
{
	public class MessageController : ApiController
	{
		private readonly IMessageService _messageService;
		
		public MessageController(IMessageService messageService)
		{
			_messageService = messageService;
		}

		// MAIN ENTRY POINT!

		public ResponseMessage Get()
		{
			return _messageService.SendMessage(new IncomeMessage());
		}

		[HttpPost]
		public ResponseMessage Send(IncomeMessage message)
		{
			return _messageService.SendMessage(message);
		}
	}
}