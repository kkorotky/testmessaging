﻿using Test.Messaging.Data.Base;
using Test.Messaging.Data.Dto;

namespace Test.Messaging.Data
{
	public interface IMessageRepository : IEntityRepository<MessageDto>
	{
	}
}
