﻿using Test.Messaging.Data.Base;
using Test.Messaging.Data.Dto;

namespace Test.Messaging.Data.Implementation
{
	public class MessageRepository : BaseInMemoryEntityRepository<MessageDto>, IMessageRepository
	{
	}
}
