﻿using System.Collections.Generic;
using Test.Messaging.Data.Base;

namespace Test.Messaging.Data.Dto
{
	public class MessageDto : IBaseEntity
	{
		public string Id { get; set; }
		public List<string> Recipients { get; set; }
		public string Subject { get; set; }
		public string Body { get; set; }

		/// <summary>
		/// true if message has been delivered by notification service
		/// </summary>
		public bool IsSend { get; set;}

		public bool IsSendFailed { get; set; }
		public string LastSendError { get; set; }
	}
}
