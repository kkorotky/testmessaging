﻿
using System.Collections.Generic;

namespace Test.Messaging.Data.Base
{
	public interface IEntityRepository<T> where T : IBaseEntity
	{
		void Add(T item);
		void Update(T item);

		T GetById(string id);

		List<T> GetAll();
	}
}
