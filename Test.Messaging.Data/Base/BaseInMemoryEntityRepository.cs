﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Test.Messaging.Data.Base
{
	public abstract class BaseInMemoryEntityRepository<T> : IEntityRepository<T> where T : IBaseEntity
	{
		// Yeah simple stratic dictionary here for storing inmemory
		private static readonly Dictionary<string, T> Items = new Dictionary<string, T>();

		public void Add(T item)
		{
			Items[item.Id] = item;
		}

		public List<T> GetAll()
		{
			return Items.Select(s => s.Value).ToList();
		}

		public T GetById(string id)
		{
			if (Items.ContainsKey(id))
			{
				return Items[id];
			}

			throw new Exception($"Item with ID {id} not found!");
		}

		public void Update(T item)
		{
			Items[item.Id] = item;
		}
	}
}
