﻿
namespace Test.Messaging.Data.Base
{
	public interface IBaseEntity
	{
		string Id { get; set; }
	}
}
